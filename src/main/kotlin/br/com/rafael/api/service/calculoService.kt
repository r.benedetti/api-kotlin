
// Ao criar o arquivo package service
package br.com.rafael.api.service
import org.springframework.stereotype.Service

//Determinar que trata-se de um Serviço
@Service
class calculoService {
    fun somar(numero: Int, numero2: Int): Int {
        return (numero + numero2)
    }

    fun subtrair(numero: Int, numero2: Int): Int {
        return (numero - numero2)
    }


    fun multiplicar(numero: Int, numero2: Int): Int {
        return (numero * numero2)
    }

    fun dividir(numero: Int, numero2: Int): Int {
        return (numero / numero2)
    }

    fun potencia(numero: Double, numero2: Double): Double {
        return Math.pow(numero, numero2)
    }

    fun raizq(numero: Double): Double {
        return Math.sqrt(numero)
    }
}