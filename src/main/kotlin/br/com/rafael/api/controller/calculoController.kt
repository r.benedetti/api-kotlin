package br.com.rafael.api.controller

//Importando os serviços para utilizar no Controller

import br.com.rafael.api.service.*
import org.springframework.web.bind.annotation.*


//Os comandos abaixo são criados quando usamos o nome deles.


@RestController
class calculoController (private val calculoService: calculoService) {

    @GetMapping("somar/{numero}/{numero2}")
    fun somar(@PathVariable numero: Int, @PathVariable numero2: Int): Int {
        return calculoService.somar(numero, numero2)
    }

    @GetMapping("subtrair/{numero}/{numero2}")
    fun subtrair(@PathVariable numero: Int, @PathVariable numero2: Int): Int {
        return calculoService.subtrair(numero, numero2)
    }

    @GetMapping("multiplicar/{numero}/{numero2}")
    fun multiplicar(@PathVariable numero: Int, @PathVariable numero2: Int): Int {
        return calculoService.multiplicar(numero, numero2)
    }

    @GetMapping("dividir/{numero}/{numero2}")
    fun dividir(@PathVariable numero: Int, @PathVariable numero2: Int): Int {
        return calculoService.dividir(numero, numero2)
    }

    @GetMapping("potencia/{numero}/{numero2}")
    fun potencia(@PathVariable numero: Double, @PathVariable numero2: Double): Double {
        return calculoService.potencia(numero, numero2)
    }

    @GetMapping("raizq/{numero}")
    fun raizq(@PathVariable numero: Double): Double {
        return calculoService.raizq(numero)
    }
}


